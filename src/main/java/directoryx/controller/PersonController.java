package directoryx.controller;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.neo4j.config.EnableNeo4jRepositories;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.data.neo4j.core.GraphDatabase;

import java.util.ArrayList;
import java.util.List;
import directoryx.model.Person;

@RestController
@RequestMapping(value = "/person")
public class PersonController {

    private List<Person> peopleList;

    @Autowired
    GraphDatabase graphDataBase;

    @Configuration
    @EnableNeo4jRepositories(basePackages = "hello")
    static class ApplicationConfig extends Neo4jConfiguration {

        public ApplicationConfig() {
            setBasePackage("hello");
        }

        @Bean
        GraphDatabaseService graphDatabaseService() {
            //"accessigndataneo4j.db" needs to change to our neo4j server for production, this is only for testing
            return new GraphDatabaseFactory().newEmbeddedDatabase("http://localhost:7474");
        }
    }

    @RequestMapping("/search?q={query}")
    public ModelAndView searchPeople(@PathVariable(value="query") String query) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("search");
        mav.addObject("people", peopleList);
        return mav;
    }

    @RequestMapping("/id")
    public Person person(@RequestParam(value="id", defaultValue="1") String id) {
        return new Person("12","swells","Steven","Wells","swells@fakemail.com","4","Software Architect","Engineering","617-000-0012","781-000-0012", null, null, null);
    }


    @RequestMapping(value = "/{userId}", method=RequestMethod.GET)
    public ModelAndView getPersonByUserId(@PathVariable(value="userId") String userId) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("profile");

        Person p = getPerson(userId);
        mav.addObject("person", p);

        return mav;
    }


    private Person getPerson(String userId) {
        Person p = null;
        if(peopleList == null) initPeopleList();
        for (Person person : peopleList) {
            if( person.getUserId().equals(userId)) {
                p = person;
                break;
            }
        }
        return p;
    }

    private List<Person> mockGetReports(String id) {
        if(peopleList == null) initPeopleList();
        List<Person> rList = new ArrayList<>();
        for (Person person : peopleList) {
            if( person.getManagerId().equals(id)) {
                rList.add(person);
            }
        }
        return rList;
    }

    private List<Person> mockFindPeople(String searchString) {
        if(peopleList == null) initPeopleList();
        List<Person> rList = new ArrayList<>();
        for(Person person : peopleList) {
            if(person.getUserId().contains(searchString) || person.getFirstName().contains(searchString) || person.getLastName().contains(searchString)) {
                rList.add(person);
            }
        }
        System.out.println(searchString);
        return rList;
    }

    private Person mockChangePersonFirstName(String id, String name) {
        Person p = null;
        if(peopleList == null) initPeopleList();
        List<Person> rList = new ArrayList<Person>();
        for(Person person : peopleList) {
            if(person.getPersonId().equals(id) || person.getUserId().equals(id)) {
                person.setFirstName(name);
                p = person;
                break;
            }
        }
        return p;
    }

    private void initPeopleList() {
        peopleList = new ArrayList<>();
        peopleList.add(new Person("1","jking","James","King","jking@fakemail.com","0","President and CEO","Corporate","617-000-0001","781-000-0001", null, null, null));
        peopleList.add(new Person("2","jtaylor","Julie","Taylor","jtaylor@fakemail.com","1","VP of Marketing","Marketing","617-000-0002","781-000-0002", null, null, null));
        peopleList.add(new Person("3","elee","Eugene","Lee","elee@fakemail.com","1","CFO","Accounting","617-000-0003","781-000-0003", null, null, null));
        peopleList.add(new Person("4","jwilliams","John","Williams","jwilliams@fakemail.com","1","VP of Engineering","Engineering","617-000-0004","781-000-0004", null, null, null));
        peopleList.add(new Person("5","rmoore","Ray","Moore","rmoore@fakemail.com","1","VP of Sales","Sales","617-000-0005","781-000-0005", null, null, null));
        peopleList.add(new Person("6","pjones","Paul","Jones","pjones@fakemail.com","4","QA Manager","Engineering","617-000-0006","781-000-0006", null, null, null));
        peopleList.add(new Person("7","pgates","Paula","Gates","pgates@fakemail.com","4","Software Architect","Engineering","617-000-0007","781-000-0007", null, null, null));
        peopleList.add(new Person("8","lwong","Lisa","Wong","lwong@fakemail.com","2","Marketing Manager","Marketing","617-000-0008","781-000-0008", null, null, null));
        peopleList.add(new Person("9","gdonovan","Gary","Donovan","gdonovan@fakemail.com","2","Marketing Manager","Marketing","617-000-0009","781-000-0009", null, null, null));
        peopleList.add(new Person("10","kbyrne","Kathleen","Byrne","kbyrne@fakemail.com","5","Sales Representative","Sales","617-000-0010","781-000-0010", null, null, null));
        peopleList.add(new Person("11","ajones","Amy","Jones","ajones@fakemail.com","5","Sales Representative","Sales","617-000-0011","781-000-0011", null, null, null));
        peopleList.add(new Person("12","swells","Steven","Wells","swells@fakemail.com","4","Software Architect","Engineering","617-000-0012","781-000-0012", null, null, null));
    }

}
