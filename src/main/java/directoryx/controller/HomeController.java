package directoryx.controller;
 
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
 
@Controller
class HomeController {
 
    @RequestMapping("/dashboard")
    String index() {
        return "index";
    }

}