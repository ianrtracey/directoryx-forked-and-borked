package directoryx.model;

import java.util.HashSet;
import java.util.Set;


import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

@NodeEntity
public class Person {

    @GraphId Long id;
    private String personId;
    private String userId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private String managerId;
    private String title;
    private String department;
    private String mobilePhone;
    private String officePhone;
    private String status;
    private String timezone;
    private String location;

    public Person(String personId, String userId, String firstName, String lastName, String emailAddress, String managerId, String title, String department, String mobilePhone, String officePhone, String status, String timezone, String location) {
        this.personId = personId;
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.managerId = managerId;
        this.title = title;
        this.department = department;
        this.mobilePhone = mobilePhone;
        this.officePhone = officePhone;
        this.status = status;
        this.timezone = timezone;
        this.location = location;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public void setOfficePhone(String officePhone) {
        this.officePhone = officePhone;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getUserId() { return userId; }

    public String getLastName() { return lastName; }

    public String getEmailAddress() { return emailAddress; }

    public String getPersonId() { return personId; }

    public String getFirstName() { return firstName; }

    public String getManagerId() { return managerId; }

    public String getTitle() { return title; }

    public String getDepartment() { return department; }

    public String getMobilePhone() { return mobilePhone; }

    public String getOfficePhone() { return officePhone; }

    public String getStatus() { return status; }

    public String getTimezone() { return timezone; }

    public String getLocation() { return location; }

}