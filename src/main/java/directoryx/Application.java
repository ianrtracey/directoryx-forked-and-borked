package directoryx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableNeo4jRepositories
public class Application {


//    public Application() {
//        setBasePackage("model");
//    }
//
//    @Bean(destroyMethod = "shutdown")
//    public GraphDatabaseService graphDatabaseService() {
//        return new GraphDatabaseFactory().newEmbeddedDatabase("hello.db");
//    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
