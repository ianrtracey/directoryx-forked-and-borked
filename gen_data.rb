require 'rubygems'
require 'json'
require 'FFaker'
require 'set'
require 'benchmark'
require 'csv'

# userid -- (see algorithm below)
# title -- FFaker::Job.title
# phoneNumber -- FFaker::PhoneNumber[AU,DE,FR,MX]
# email -- userid@directoryx.com
# location -- FFaker::Address #{city, country}
# ManagerID -- (see algorithm below)

DATASETSIZE = 100_000

# userid Algorithm -- takes first two letters of first name and first 5 letters of last name.
# 	If there is a collision, you continue to add a random number to the end
class UserId
def initialize
	@unique_user_ids = Set.new
end
def generate_userid(first,last)
	puts "generating user id..."
	if first.nil?
		first = "none"
	end
	if last.nil?
		last = "none"
	end
	userid = first[0..1]+last[0..4]
	while @unique_user_ids.include?(userid)
		userid = userid + Random.rand(0..100).to_s
	end
	puts "done"
	@unique_user_ids.add(userid)
	return userid.downcase
end
end

# Takes the first person and makes that person the CEO
# looks at the first subset of 5,000 people, making 10% managers randomly
# links the remainding people to the managers by passing over each manager and randomly
# selecting between 100..500 at each pass
# shoves the managers into a hash starting with a 'level-1' relationship, linking the first
# managers to the CEO
class ReportingChain
	def initialize
		@current_managers = []
		@ceo = nil
		@manager_percentage = 0.90
		@reporting_chain = []
	end

	def generate_reporting_chain(people)
		@ceo = people.slice!(0)
		@reporting_chain << @ceo
		# handles the first level manually in order to link to CEO
		puts "Level 1"
		level = people.slice!(0..DATASETSIZE/20.floor)
		managers = level.slice!(0..((@manager_percentage*level.size).floor))
		managers.each do |manager|
		  manager.merge!(managerId: @ceo[:userid])
		end
		@current_managers = managers
		@reporting_chain += @current_managers
		level.each do |l|
		 l.merge!(managerId: @current_managers[Random.rand(0..@current_managers.size-1)][:userid])
		 @reporting_chain << l
		end
		@manager_percentage -= 0.04
		i = 1
		while !people.empty?
			i += 1
			puts "Level #{i}"
			if people.size < DATASETSIZE/20.floor
			level = people.slice!(0..people.size)
			else
			level = people.slice!(0..DATASETSIZE/20.floor)
			end
			managers = level.slice!(0..((@manager_percentage*level.size).floor))
				managers.each do |manager|
		  		manager.merge!(managerId: @current_managers[Random.rand(0..@current_managers.size-1)][:userid])
				end
			@current_managers = managers
			@reporting_chain += @current_managers
			level.each do |l|
		 	l.merge!(managerId: @current_managers[Random.rand(0..@current_managers.size-1)][:userid])
			 @reporting_chain << l
			end
			@manager_percentage -= 0.04
		end
		return @reporting_chain
	end
end




begin_time = Time.now
puts "parsing json file...."
file = File.read('../Desktop/export_names.json')
data_hash = JSON.parse(file)
puts "done"

first_names = []
last_names = []

puts "building arrays...."
i = 1
data_hash['items'].each do |name|
	i += 1
	puts "#{i}"
	first_names << name['first_name']
	last_names << name['last_name']
end
puts "done"


shuffled_data_hash = []
puts "shuffling first name and last name data...."
i = 0
first_names_size = first_names.size.to_i
last_names_size = last_names.size.to_i
uid = UserId.new
DATASETSIZE.times do
	i += 1
	puts "#{i}"
	first_rand = Random.rand(0..first_names_size)
	last_rand = Random.rand(0..last_names_size)
	puts "adding userid, location, phonenumber and email attributes..."
	userid = uid.generate_userid(first_names[first_rand],last_names[last_rand])
	shuffled_data_hash << {:first_name => first_names[first_rand],:last_name => last_names[last_rand],:userid=>userid,:location=>"#{FFaker::Address.city}, #{FFaker::Address.country}",:phone=>FFaker::PhoneNumber.phone_number,:email=>userid+"@directoryx.com"}
	first_names.delete_at(first_rand)
	last_names.delete_at(last_rand)
	first_names_size -= 1
	last_names_size -= 1
end
puts "done"

puts "creating reporting chain..."
reporting_chain = ReportingChain.new
data = reporting_chain.generate_reporting_chain(shuffled_data_hash)




puts "writing file to json..."
File.open('directoryx_sample.json', 'w') do |f|
	f.puts data.to_json
end
puts "done"
puts "converting file to csv..."
csv_string = CSV.generate do |csv|
	csv << ["first_name","last_name","userid","location","phone","email", "managerId"]
  JSON.parse(data.to_json).each do |hash|
    csv << hash.values
  end
end
puts "writing file to csv..."
File.open('directoryx_sample.csv', 'w') do |f|
	f.puts csv_string
end
end_time = Time.now
puts "Time taken is #{end_time - begin_time}."

